<?php

/*
 * Main app
 */

// include dependencies
require __DIR__ . '/../vendor/autoload.php';

// Slim Config
$slimConfig = require __DIR__ . '/../app/config.php';

// Init slim app
$app = new \Slim\App($slimConfig);

// Get slim container
$container = $app->getContainer();

// Eloquent Database
$capsule = new \Illuminate\Database\Capsule\Manager;

$capsule->addConnection($slimConfig['settings']['db']);

$capsule->setAsGlobal();

$capsule->bootEloquent();

$container['db'] = function($container) use ($capsule) {
    $capsule;
};

// Twig View
$container['view'] = function($container) {

    global $slimConfig;

    $view = new \Slim\Views\Twig(__DIR__ . '/../views', [
        'cache' => $slimConfig['settings']['twigCache'],
    ]);

    $view->addExtension(new \Slim\Views\TwigExtension(
        $container->router,
        $container->request->getUri()
    ));

    return $view;
};

// 404
$container['notFoundHandler'] = function($container) {
    return function ($request, $response) use ($container) {
        return $container->view->render($response, '404.twig.html');
    };
};

// Controller Setup

// LeaderBoard Controller
$container['LeaderBoardController'] = function($container) {
    return new \App\Controllers\LeaderBoardController($container);
};
// API Controller
$container['APIController'] = function($container) {
    return new \App\Controllers\APIController($container);
};

// Include routes
require __DIR__ . '/../app/routes.php';