<?php

// Config
return [
    'settings' => [
        // DEBUG
        'displayErrorDetails'   => true,
        'debug'                 => true,

        // DATABASE INFO
        'db' => [
            'driver'    => 'mysql',
            'host'      => 'localhost',
            'database'  => 'tournament',
            'username'  => 'root',
            'charset'   => 'utf8',
            'collation' => 'utf8_unicode_ci',
            'prefix'    => '',
        ],

        // TwigPHP Cache
        'twigCache' => false,
    ],
];