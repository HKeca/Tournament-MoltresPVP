<?php

$slimConfig = require __DIR__ . '/config.php';

if ($slimConfig['settings']['debug'])
{
    /*
     * Tournament routes
     */
    $app->get('/', 'LeaderBoardController:getTournaments')->setName('tournaments.home');

    $app->get('/tournaments/{tournamentId}', 'LeaderBoardController:getTournament')->setName('tournament.stats');

    $app->get('/live', 'LeaderBoardController:getLiveTournament')->setName('tournament.live');
    /*
     * Api routes
     */
    $app->get('/api/tournament/live', 'APIController:getLiveTournamentData')->setName('api.getTournamentDataLive');
    $app->get('/api/tournament/live/players', 'APIController:getLiveTournamentPlayerData')->setName('api.getTournamentPlayerDataLive');
    $app->get('/api/tournament/{tournamentID}', 'APIController:getTournamentData')->setName('api.getTournamentData');
}
else
{
    /*
     * Tournament routes
     */
    $app->get('/playerstats/tournament/', 'LeaderBoardController:getTournaments')->setName('tournaments.home');

    $app->get('/playerstats/tournament/tournaments/{tournamentId}', 'LeaderBoardController:getTournament')->setName('tournament.stats');

    $app->get('/playerstats/tournament/live', 'LeaderBoardController:getLiveTournament')->setName('tournament.live');
    /*
     * Api routes
     */
    $app->get('/playerstats/tournament/api/tournament/live', 'APIController:getLiveTournamentData')->setName('api.getTournamentDataLive');
    $app->get('/playerstats/tournament/api/tournament/live/players', 'APIController:getLiveTournamentPlayerData')->setName('api.getTournamentPlayerDataLive');
    $app->get('/playerstats/tournament/api/tournament/{tournamentID}', 'APIController:getTournamentData')->setName('api.getTournamentData');
}