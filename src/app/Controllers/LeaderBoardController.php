<?php
/*
 * Leader board controller
 */

namespace App\Controllers;


// DB Models
use App\Models\Tournament;
use App\Models\TournamentPlayers;
use App\Models\Accounts;

// Config
$slimConfig = require __DIR__ . '/../config.php';

class LeaderBoardController extends Controller
{


    /**
     * List tournaments
     *
     * @param $request
     * @param $response
     * @return twigView | PSR4 Response
     */
    public function getTournaments($request, $response)
    {
        global $slimConfig;

        // Template Data
        $templateData = array();

        //DEBUG
        $templateData['debug'] = $slimConfig['settings']['debug'];

        // Query tables
        $tournaments = Tournament::all();

        // Put each tournament in an array
        $tournamentNames = array();

        foreach ($tournaments as $tournament)
        {
            $tournamentNames[] = $tournament->id;
        }

        // add the tournament id array to the template data
        $templateData['tournaments'] = $tournamentNames;

        // Render twig view
        return $this->view->render($response, 'tournaments.twig.html', $templateData);
    }


    /**
     * Get tournament data
     *
     * @param $request
     * @param $response
     * @param $args
     *
     * @return Twig View
     */
    public function getTournament($request, $response, $args)
    {
        global $slimConfig;

        // Template Data
        $templateData = array();

        //DEBUG
        $templateData['debug'] = $slimConfig['settings']['debug'];

        // Tournament id
        $tournamentId = $args['tournamentId'];

        // Get tournament row
        $tournament = Tournament::where('id', '=', $tournamentId)->first();

        // If tournament doesn't exist
        if (!$tournament)
            return $this->view->render($response, '404.twig.html');

        // Set tournament data
        $templateData['hostName'] = $tournament->hostname;
        $templateData['hostUuid'] = $tournament->hostuuid;
        $templateData['tournamentId'] = $tournament->id;
        $templateData['type'] = $tournament->type;
        $templateData['startTime'] = $tournament->start_time;
        $templateData['totalKills'] = $tournament->total_kills;

        // Winner accounts
        $winnerAccount1 = Accounts::where('uuid', '=', $tournament->winner)->first();

        // if no username
        if (!empty($winnerAccount1->username))
            $templateData['winner'] = $winnerAccount1->username;
        else
            $templateData['winner'] = 'N/A';

        // Winner 2
        // if winner2 is populated
        if (!$tournament->winner2 == "")
        {
            $winnerAccount2 = Accounts::where('uuid', '=', $tournament->winner2)->first();

            if (!empty($winnerAccount2->username))
                $templateData['winner2'] = $winnerAccount2->username;
            else
                $templateData['winner2'] = 'N/A';
        }

        // Get player data for that tournament
        $playerData = array();
        $players = array();

        // Get players where their tournament id matches or uuid search
        if (!empty($request->getQueryParams('player', "")['player']))
        {
            $accounts = Accounts::where('username', 'like', $request->getQueryParams('player')['player'] . '%')->get();

            if ($accounts)
            {
                $templateData['isSearch'] = '1';
                foreach ($accounts as $account)
                {
                    $data = json_decode($account);
                    $uuid = $data->uuid;
                    foreach (TournamentPlayers::where('tourney_id', '=', $tournament->id)->where('uuid', 'like', $uuid . '%')->get() as $player)
                    {
                        $players[] = $player;
                    }
                }
            }
        }
        else
        {
            $players = TournamentPlayers::where('tourney_id', '=', $tournament->id)->get();
        }

        foreach ($players as $player)
        {
            // temporary array for use data
            $tmpArray = array();

            // Get account data using user uuid
            $accountData = Accounts::where('uuid', '=', $player->uuid)->first();
            $enemyAccount = Accounts::where('uuid', '=', $player->enemy)->first();

            // Set the username
            if (!empty($accountData->username))
                $tmpArray['name'] = $accountData->username;
            else
                $tmpArray['name'] = 'N/A';
            // Set the enemy username
            if (!empty($enemyAccount->username))
                $tmpArray['enemy'] = $enemyAccount->username;
            else
                $tmpArray['enemy'] = 'N/A';

            // if teams
            // TODO: Make enum for tournament types
            if ($tournament->type == 3)
            {
                $tmpArray['team'] = $player->team;
                $tmpArray['enemyTeam'] = $player->enemy_team;
            }

            // Player Stats
            $tmpArray['uuid'] = $player->uuid;
            $tmpArray['kills'] = $player->kills;

            // Calculate time
            $scheduledTime = new \DateTime('1970');
            $scheduledTime->modify('+' . $player->scheduledTime . ' minutes');

            $tmpArray['scheduledTime'] = $scheduledTime->format('h:i A m/d/Y');
            $tmpArray['bracket'] = $player->bracket;

            $playerData[] = $tmpArray;
        }

        $templateData['playerData'] = $playerData;

        return $this->view->render($response, 'tournament.twig.html', $templateData);
    }

    /**
     * Render view for live tournament
     *
     * @param $request
     * @param $response
     * @param $args
     * @return Twig view
     */
    public function getLiveTournament($request, $response, $args)
    {
        global $slimConfig;

        // Template data
        $templateData = array();

        $templateData['debug'] = $slimConfig['settings']['debug'];

        // get tournament type
        $tournamentType = Tournament::orderBy('id', 'DESC')->first()->type;
        $templateData['type'] = $tournamentType;

        return $this->view->render($response, 'liveTournament.twig.html', $templateData);
    }
}