<?php

namespace App\Controllers;

class Controller
{
    // Slim container access
    protected $container;

    /**
     * Controller constructor.
     * @param $container
     */
    public function __construct($container)
    {
        $this->container = $container;
    }

    /**
     * Allow easy access to slim container
     * @param $prop
     * @return mixed
     */
    public function __get($prop)
    {
        if ($this->container->{$prop})
            return $this->container->{$prop};
    }
}