<?php
/*
 * API controller
 */

namespace App\Controllers;

// DB Models
use App\Models\Accounts;
use App\Models\Tournament;
use App\Models\TournamentPlayers;

class APIController extends Controller
{
    /**
     * Get tournament data and return it
     *
     * @param $request
     * @param $response
     * @param $args
     * @return Json Response
     */
    public function getTournamentData($request, $response, $args)
    {
        $tournamentID = $args['tournamentID'];

        $tournamentQueryData = Tournament::where('id', '=', $tournamentID)->first();

        if (!$tournamentQueryData)
        {
            $jsonResponse = $response->withJson(json_encode(array('error' => 'Tournament does not exist.')));
            return $jsonResponse;
        }

        $tournamentData = json_encode($tournamentQueryData);

        $jsonResponse = $response->withJson($tournamentData);

        return $jsonResponse;
    }

    /**
     * Get live tournament data
     *
     * @param $request
     * @param $response
     * @param $args
     * @return json response
     */
    public function getLiveTournamentData($request, $response, $args)
    {
        $tournamentQuery = Tournament::orderBy('id', 'DESC')->first();

        $jsonResponse = $response->withJson(json_encode($tournamentQuery));

        return $jsonResponse;
    }


    /**
     * Get player data for live tournament
     *
     * @param $request
     * @param $response
     * @param $args
     * @return json response
     */
    public function getLiveTournamentPlayerData($request, $response, $args)
    {
        $tournamentID = Tournament::orderBy('id', 'DESC')->first()->id;

        if (!$tournamentID)
        {
            $jsonResponse = $response->withJson(json_encode(array('error', 'Tournament not found')));
            return $jsonResponse;
        }

        $players = TournamentPlayers::where('tourney_id', '=', $tournamentID)->get();

        $playerNames = array();
        foreach ($players as $player)
        {
            $playerNames[] = Accounts::where('uuid', '=', $player->uuid)->first()->username;
        }

        $enemyNames = array();
        foreach ($players as $player)
        {
            $enemyName = Accounts::where('uuid', '=', $player->enemy)->first();
            if ($enemyName)
                $enemyNames[] = $enemyName->username;
        }

        $tournamentData = array('playerNames' => $playerNames, 'enemyNames' => $enemyNames, 'playerData' => $players);

        $jsonResponse = $response->withJson(json_encode($tournamentData));

        return $jsonResponse;
    }
}
