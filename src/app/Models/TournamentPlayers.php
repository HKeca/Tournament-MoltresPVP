<?php
/**
 * Tournament players model
 */
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TournamentPlayers extends Model
{
    // Table name
    protected $table = "TournamentPlayers";
}