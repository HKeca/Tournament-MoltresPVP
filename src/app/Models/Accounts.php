<?php
/**
 * Accounts Model
 */
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Accounts extends Model
{
    // Table name
    protected $table = "accounts";
}