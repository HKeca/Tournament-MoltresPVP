<?php

/**
 * tournaments model
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Tournament extends Model
{
    // Table name
    protected $table = "tournament";
}