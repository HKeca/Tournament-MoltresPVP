var searchingPlayers = [];

// TODO: Add team ajax support

$(document).ready(function() {
    var liveApi = "http://localhost/public/api/tournament/live";
    var playerApi = "http://localhost/public/api/tournament/live/players";
    var lastData = {};
    var tournamentType = "";


    // Player Search
    $(".player-search").on("input", function() {

        var search = $(this).val();

        if (search != "")
            search = search + "*";

        playerSearch(search);
    });

    // Api calls
    $.ajax({
        url: liveApi
    }).done(function(data) {
        var data = JSON.parse(data);

        $("#tournament-id").html(data.id);
        $(".tournament-type").html(parseType(data.type));
        $("#total-kills").html(data.total_kills);
        $("#host-image").attr("src", "https://crafatar.com/avatars/" + data.hostuuid + "?size=90");
        $("#host-name").html(data.hostname);

        tournamentType = data.type;

    }).fail(function() {
        console.error('Failed to get tournament data');
    });

    $.ajax({
        url: playerApi
    }).done(function(data) {
        var data = JSON.parse(data);

        lastData = data;

        data.playerData.forEach(function(currValue, index) {

            var scheduledTime = moment("1-1-1970", "DD-MM-YYYY");
            scheduledTime = scheduledTime.add(currValue.scheduledTime, 'm');
            scheduledTime = scheduledTime.format('hh:mm A M/D/YYYY');

            var row = "";
            row += "<tr>";
            row += "<td><img src='https://crafatar.com/avatars/" + currValue.uuid + "?size=60' alt='player head'></td>";
            row += "<td class='playerName' id='" + data.playerNames[index] + "'>" + data.playerNames[index] + "</td>";
            row += "<td>" + currValue.kills + "</td>";
            if (currValue.dead == 0)
            {
                row += "<td>" + scheduledTime + "</td>";
            }
            row += "<td>" + (data.enemyNames[index] ? data.enemyNames[index] : 'N/A') + "</td>";
            row += "<td>" + currValue.bracket + "</td>";
            if (tournamentType == 3)
            {
                row += "<td>" + currValue.team + "</td>";
                row += "<td>" + currValue.enemy_team + "</td>";
            }
            row += "</tr>";

            if (currValue.dead == 1)
            {
                $("#deadPlayerDataBody").append(row);
            }
            else
            {
                $("#playerDataBody").append(row);
            }
        });
    }).fail(function() {
        console.error("failed to get player data");
    });

    setInterval(function() {
        $.ajax({
            url: liveApi
        }).done(function(data) {
            var data = JSON.parse(data);

            $("#tournament-id").html(data.id);
            $(".tournament-type").html(parseType(data.type));
            $("#total-kills").html(data.total_kills);
            $("#host-image").attr("src", "https://crafatar.com/avatars/" + data.hostuuid + "?size=90");
            $("#host-name").html(data.hostname);

            tournamentType = data.type;

        }).fail(function() {
            console.error('Failed to get tournament data');
        });

        $.ajax({
            url: playerApi
        }).done(function(data) {

            // Check if data has changed
            if (_.isEqual(data, lastData))
            {
               return;
            }


            var data = JSON.parse(data);

            lastData = data;

            clearTables();

            data.playerData.forEach(function(currValue, index) {
                    if ($.inArray(data.playerNames[index], searchingPlayers) < 0) {

                        var scheduledTime = moment("1-1-1970", "DD-MM-YYYY");
                        scheduledTime = scheduledTime.add(currValue.scheduledTime, 'm');
                        scheduledTime = scheduledTime.format('hh:mm A M/D/YYYY');

                        var row = "";
                        row += "<tr>";
                        row += "<td><img src='https://crafatar.com/avatars/" + currValue.uuid + "?size=60' alt='player head'></td>";
                        row += "<td class='playerName' id='" + data.playerNames[index] + "'>" + data.playerNames[index] + "</td>";
                        row += "<td>" + currValue.kills + "</td>";
                        if (currValue.dead == 0)
                        {
                            row += "<td>" + scheduledTime + "</td>";
                        }
                        row += "<td>" + (data.enemyNames[index] ? data.enemyNames[index] : 'N/A') + "</td>";
                        row += "<td>" + currValue.bracket + "</td>";
                        if (tournamentType == 3)
                        {
                            row += "<td>" + currValue.team + "</td>";
                            row += "<td>" + currValue.enemy_team + "</td>";
                        }
                        row += "</tr>";

                        if (currValue.dead == 1)
                            $("#deadPlayerDataBody").append(row);
                        else
                            $("#playerDataBody").append(row);
                    }
            });
        }).fail(function() {
            console.error("failed to get player data");
        });
    }, 1000);
});

function parseType(type) {
    if (type == 1)
        return "Elimination";
    else if (type == 2)
        return "Free For All";
    else if (type == 3)
        return "Teams"
    else
        return "N/A";
}

function playerSearch(playerName) {
    $(".playerName").each(function() {


        var regExStr = new RegExp("^" + playerName.split('*').join(".*") + "$");

        if (regExStr.test($(this).text()))
        {
            if ($.inArray($(this).text(), searchingPlayers) < 0)
                searchingPlayers.push($(this).text());
            $(this).parent().css("background-color", "f1c40f");
        }
        else
        {
            if ($.inArray($(this).text(), searchingPlayers) > -1)
                searchingPlayers.splice($(this).text(), 1);

            $(this).parent().attr("isSearch", "false");
            $(this).parent().css("background-color", "f9f9f9");
        }

    });
}

function clearTables() {
    $(".playerName").each(function() {
        if ($.inArray($(this).text(), searchingPlayers) > -1)
        {
        }
        else
        {
            $(this).parent().remove();
        }
    });
}