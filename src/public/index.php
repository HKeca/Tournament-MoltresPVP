<?php

/*
 * Include main app
 *
 */
require __DIR__ . '/../bootstrap/app.php';

/*
 * Run slim app
 *
 */
$app->run();